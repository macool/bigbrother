class AddPreferencesToUser < ActiveRecord::Migration
  def up
    change_table :users do |t|
      t.boolean :share_automatically, default: true
    end
  end
  def down
    change_table :users do |t|
      t.remove :share_automatically
    end
  end
end
