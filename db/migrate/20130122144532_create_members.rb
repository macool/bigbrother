class CreateMembers < ActiveRecord::Migration
  def change
    create_table :members do |t|
      t.string :nombre, null: false
      t.string :username, default: nil
      t.string :lista, null: false
    end
  end
end
