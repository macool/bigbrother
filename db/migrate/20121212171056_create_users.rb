class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :token
      t.string :screen_name
      t.string :name
      t.string :image
      t.string :description
      t.string :url
      t.string :email
      
      # twitter fields:
      t.datetime :twitter_created_at
      t.string :twitter_access
      t.string :twitter_secret
      t.boolean :follows_loxaesmas
      
      # facebook fields:
      t.string :facebook_access
      t.boolean :likes_loxaesmas
    end
    add_index :users, :token, unique: true
  end
end
