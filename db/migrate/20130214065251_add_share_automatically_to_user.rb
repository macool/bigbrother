class AddShareAutomaticallyToUser < ActiveRecord::Migration
  def change
    add_column :users, :share_automatically, :boolean, :default => true
  end
end
