class MakePreguntaBelongToMember < ActiveRecord::Migration
  def up
    change_table :pregunta do |t|
      t.remove :dirigida_a
      t.references :member
    end
  end

  def down
    change_table :pregunta do |t|
      t.string :dirigida_a
      t.remove :member_id
    end
  end
end
