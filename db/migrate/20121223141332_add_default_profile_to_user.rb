class AddDefaultProfileToUser < ActiveRecord::Migration
  def change
    add_column :users, :default_profile, :string
  end
end
