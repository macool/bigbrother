class CreatePregunta < ActiveRecord::Migration
  def change
    create_table :pregunta do |t|
      t.string :titulo
      t.text :detalle
      t.string :enlace

      t.references :user
      t.timestamps
    end
  end
end
