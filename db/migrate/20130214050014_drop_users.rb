class DropUsers < ActiveRecord::Migration
  def up
    remove_index :users, :token
    drop_table :users
  end

  def down
    create_table :users do |t|
      t.string   "token"
      t.string   "screen_name"
      t.string   "name"
      t.string   "image"
      t.string   "description"
      t.string   "url"
      t.string   "email"
      t.datetime "twitter_created_at"
      t.string   "twitter_access"
      t.string   "twitter_secret"
      t.boolean  "follows_loxaesmas"
      t.string   "facebook_access"
      t.boolean  "likes_loxaesmas"
      t.integer  "twitter_followers_count"
      t.integer  "facebook_friends_count"
      t.string   "default_profile"
      t.boolean  "share_automatically",     :default => true
    end
    add_index "users", ["token"], :name => "index_users_on_token", :unique => true
  end
end
