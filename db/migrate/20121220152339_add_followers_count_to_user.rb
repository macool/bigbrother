class AddFollowersCountToUser < ActiveRecord::Migration
  def change
    add_column :users, :twitter_followers_count, :integer
  end
end
