# encoding: utf-8
class AddMembers < ActiveRecord::Migration
  def up
    names = [
      "Willian Ludeña Celi",
      "Fredy Bravo",
      "Yadira Flores Obaco",
      "Geovanny Loaiza",
      "Fausto Moreno Sánchez",
      "Miryam González Serrano",
      "Raul Auquilla Ortega",
      "Juan Carlos Ríos Espinoza",
      "José Bolívar Castillo V",
      "Nivea Vélez",
      "Todos"
    ]
    parties = [
      "3",
      "7",
      "8",
      "10",
      "15-18",
      "17-35",
      "21",
      "23",
      "61",
      "62",
      ""
    ]
    usernames = [
      "",
      "@freddybravob",
      "@Yadiraavanza",
      "",
      "@Miryamoficial",
      "@RaulAuquillaO",
      "",
      "@chatojobol",
      "@NiveaLuzVelez",
      "",
      ""
    ]
    for i in 0..10 do
      m = Member.new
      m.nombre = names[i]
      m.username = usernames[i]
      m.lista = parties[i]
      m.save!
    end
  end

  def down
    Member.all.each do |m|
      m.destroy
    end
  end
end
