# encoding: utf-8

require 'net/http'

class SessionsController < ApplicationController
  
  def index
    @place = "iniciar_sesion"
    if current_user
      redirect_to root_url
      flash["alert-info"] = "ya has iniciado sesión"
      return
    end
  end

  def create
    user = User.create_or_update_from_omniauth(request.env["omniauth.auth"])
    session[:user_id] = user.id
    redirect_to root_path
  end
  
  def destroy
    session[:user_id] = nil
    session[:has_asked_question] = nil
    flash["alert-info"] = "has cerrado tu sesión. Esto no cierra tu sesión dentro de facebook o twitter."
    redirect_to root_url
  end
  
  private
  
  def is_valid_user?
    valid = false
    if (not current_user.twitter_created_at.blank? and not current_user.twitter_followers_count.blank?) and Time.now - current_user.twitter_created_at > 30.days and current_user.twitter_followers_count > 20
      valid = true
    end
    if (not current_user.facebook_access.blank?) and current_user.facebook_friends_count > 40
      valid = true
    end
    valid
  end
  
end
