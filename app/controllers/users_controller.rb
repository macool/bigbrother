# encoding: utf-8
class UsersController < ApplicationController
  
  before_filter :confirm_logged_in

  def me
    @place = "mis_preferencias"
  end

  def update_preferences
    current_user.share_automatically = params[:user][:share_automatically]
    current_user.save validate: false
    flash["alert-info"] = "Tus preferencias han sido actualizadas."
    if current_user.share_automatically
      flash["alert-info"] += " Tu actividad se compartirá automáticamente con Facebook o Twitter."
    else
      flash["alert-info"] += " Tu actividad no se compartirá automáticamente con Facebook o Twitter."
    end
    respond_to do |format|
      format.html { redirect_to :back }
      format.js
    end
  end

end
