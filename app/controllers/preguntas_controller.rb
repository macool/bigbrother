# encoding: utf-8
class PreguntasController < ApplicationController
  
  before_filter :confirm_logged_in, :only => :me

  def index
    if session[:has_asked_question]
      begin
        @pregunta = Pregunta.new JSON.parse(session[:has_asked_question])
      rescue 
        @pregunta = Pregunta.new
      end
      @place = "preguntas_new"
      @auto_submit_form = true
      session[:has_asked_question] = nil
      render :new
    else
      @preguntas_pag = Pregunta.page(params[:page]).per(10)
      @preguntas = @preguntas_pag.includes(:user, :member).find_with_reputation(:votes, :all, order: "votes desc")
      @place = "preguntas_index"
    end
  end

  def new
    @place = "preguntas_new"
    if session[:has_asked_question]
      @pregunta ||= Pregunta.new JSON.parse(session[:has_asked_question])
    else
      @pregunta ||= Pregunta.new
    end
  end

  def create
    if current_user
      @pregunta = Pregunta.new params[:pregunta]
      @pregunta.user = current_user
      if @pregunta.save
        flash["alert-success"] = "acabas de preguntar."
        link = "#{request.protocol}#{request.host_with_port}/preguntas/#{@pregunta.id}"
        if @pregunta.member.nombre == "Todos"
          message_candidato = "a todos los candidatos"
        else
          message_candidato = "al candidato #{@pregunta.member.nombre_or_nickname}"
        end
        current_user.share_to_social_networks({
          :message => "#EleccionesEC Acabo de colocar una pregunta #{message_candidato}. #{link}",
          :link => link
        })
        redirect_to action: :index
      else
        flash["alert-error"] = "hubo un problema al guardar tu pregunta. Asegúrate de haber llenado los campos requeridos."
        @place = "preguntas_new"
        render :new
      end
    else
      flash["alert-info"] = "por favor, inicia sesión para que tu pregunta sea válida"
      session[:has_asked_question] = params[:pregunta].to_json
      redirect_to login_url
    end
  end
  
  def show
    @pregunta = Pregunta.find(params[:id], include: :user)
    case Pregunta.count
    when 1..4
      limit = 1
    else
      limit = 2
    end
    no_populares = Pregunta.includes(:user).find_with_reputation(:votes, :all, order: "votes asc, id desc", limit: (limit+1))
    populares = Pregunta.includes(:user).find_with_reputation(:votes, :all, order: "votes desc, id desc", limit: limit)
    @preguntas = no_populares.zip(populares).flatten.compact
  end
  
  def vote
    if current_user
      if @pregunta = Pregunta.find_by_id(params[:id])
        if @pregunta.has_evaluation? :votes, current_user
          @pregunta.delete_evaluation(:votes, current_user)
        else
          if current_user.reputation_for(:user_votes).to_i >= current_user.allowed_votes_count
            flash["alert-error"] = "puedes votar hasta por #{current_user.allowed_votes_count} preguntas. Recuerda que mientras más preguntas coloques, por más preguntas puedes votar."
            @error = true
            return
          end
          @pregunta.add_or_update_evaluation(:votes, 1, current_user)
        end
      end
    end
    respond_to do |format|
      format.html do
        redirect_to :back 
      end
      format.js do
        render 'vote'
      end
    end
  end
  
  def share
    if current_user
      @pregunta = Pregunta.find params[:id], :include => [:user, :member]
      link = "#{request.protocol}#{request.host_with_port}/preguntas/#{@pregunta.id}"
      if @pregunta.member.nombre == "Todos"
        message_for_candidato = "a todos los candidatos"
      else
        message_for_candidato = "al candidato #{@pregunta.member.nombre_or_nickname}"
      end
      obj = {
        :message => "#EleccionesEC me gustó esta pregunta que #{@pregunta.user.twitter_or_fb_nickname} hizo #{message_for_candidato}. #{link}",
        :link => link
      }
      current_user.share_to_social_networks(obj)
      flash["alert-success"] = "Lo hemos compartido con tus amigos."
    else
      flash["alert-error"] = "Inicia sesión antes de poder compartir."
    end
    respond_to do |format|
      format.html do 
        if current_user
          redirect_to :back
        else
          redirect_to login_url
        end
      end
      format.js
    end
  end
  
  def me
    @place = "mis_preguntas"
    @preguntas_pag = current_user.preguntas.page(params[:page]).per(10)
    @preguntas = @preguntas_pag.includes(:user, :member).find_with_reputation(:votes, :all, order: "votes desc")
  end

end
