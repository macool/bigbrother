# encoding: utf-8
class ShareController < ApplicationController

  def create
    if current_user and not ( session[:shared_to_social_networks] and ( Time.now - session[:shared_to_social_networks] ) < 5.minutes )
      if @pregunta = Pregunta.find_by_id(params[:id])
        link = "http://qoloquio.com/preguntas/#{@pregunta.id}"
        obj = {
          :message => "#EleccionesEC me gustó esta pregunta que #{@pregunta.user.twitter_or_fb_nickname} le hizo al candidato #{@pregunta.member.nombre}. #{link}",
          :link => link
        }
        current_user.share_to_social_networks(obj)
        session[:shared_to_social_networks] = Time.now
        render :text => "ok"
      else
        render :text => "nope"
      end
    else
      render :text => "nope"
    end
  end

end
