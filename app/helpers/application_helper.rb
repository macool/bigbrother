module ApplicationHelper

  def active?( name )
    if @place == name
      raw 'class="active"'
    end
  end

  def send_notification( kind, content )
    raw("window.Helpers.Notification.create({kind:'#{kind}', content:'#{content}'});")
  end

  def js_send_all_notifications
    notifications = ""
    flash.each do |key, msg|
      key_str = key.dup
      # if key_str.index("alert-")
      #   key_str.slice!( key_str.index("alert-"), 6 )
      # end
      notifications += send_notification key_str, msg
      flash.clear
    end
    raw notifications
  end

end
