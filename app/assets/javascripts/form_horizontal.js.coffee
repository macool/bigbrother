FormHorizontal = {
  init: ->
    $forms = $("form.is_form_horizontal").addClass("form-horizontal")
    $forms.find(".field").wrap("<div class='control-group' />")
    $forms.find("label").addClass("control-label")
    $forms.find("input, textarea, select").wrap("<div class='controls' />")
    
}

jQuery ->
  FormHorizontal.init()