window.Helpers.Notification = {
  create: (obj) ->
    if obj.kind is undefined
      obj.kind = ""
    html = "<div class='alert #{obj.kind}' style='display:none;'>" +
              "<a class='close' data-dismiss='alert' href='#'>x</a>" +
              obj.content +
            "</div>" 
    $("div.alerts").prepend(html)
    $("div.alerts div:first").slideDown()
    null
}