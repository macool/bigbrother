PreguntasViewController = {
  init: ->
    $(".pregunta").on "mouseenter", ->
      $(this).find(".share_pregunta, .show_pregunta").fadeIn("fast")
    $(".pregunta").on "mouseleave", ->
      $(this).find(".share_pregunta, .show_pregunta").fadeOut("fast")
}

jQuery ->
  PreguntasViewController.init()