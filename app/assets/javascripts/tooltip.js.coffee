window.Helpers = {} unless window.Helpers

window.Helpers.Tooltip = {
  init: ->
    $(".ttip").tooltip()
    null
}

jQuery ->
  window.Helpers.Tooltip.init()