window.Helpers = {} unless window.Helpers

window.Helpers.MyPreferencesViewController = {
  init: ->
    $form = $(".my_preferences_form")
    $form.find("#user_share_automatically").on "change", ->
      $form.submit()
}