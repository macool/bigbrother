# encoding: utf-8

require 'openssl'
require 'digest/sha1'

class Encode
  
  def self.encode( string )
    c = OpenSSL::Cipher::Cipher.new("aes-256-cbc")
    c.encrypt
    c.key = Digest::SHA1.hexdigest( ENV["salt"] )
    c.iv = Digest::SHA1.hexdigest( ENV["salt_iv"] )
    e = c.update( string )
    e << c.final
    URI.escape e
  end
  
  def self.decode( string )
    string = URI.unescape string
    c = OpenSSL::Cipher::Cipher.new("aes-256-cbc")
    c.decrypt
    c.key = Digest::SHA1.hexdigest( ENV["salt"] )
    c.iv = Digest::SHA1.hexdigest( ENV["salt_iv"] )
    d = c.update( string )
    d << c.final
    d
  end
  
end