class User < ActiveRecord::Base
  attr_accessible :profile

  # relations:
    has_one :facebook_profile, dependent: :destroy
    has_one :twitter_profile, dependent: :destroy
    has_many :preguntas

  # reputation:
    has_reputation :user_votes, source: {reputation: :votes, of: :preguntas}, aggregated_by: :sum

  # validations
    validates :profile, :presence => true

  # methods:
    # alias methods:
      def name
        def_profile.name
      end
      def nickname
        def_profile.nickname
      end
      def email
        if profile == "facebook"
          def_profile.email
        end
      end
      def image
        def_profile.image
      end

    def def_profile
      if self.read_attribute(:profile) == "twitter"
        @twitter_profile ||= self.twitter_profile
      elsif self.read_attribute(:profile) == "facebook"
        @facebook_profile ||= self.facebook_profile
      else
        raise "only FB and Twitter are supported"
      end
    end
    def allowed_votes_count
      if @allowed_votes_count.blank?
        @allowed_votes_count = base = 2
        case self.preguntas.count
        when 0
          base
        when 1
          @allowed_votes_count = base * 2
        when 2
          @allowed_votes_count = base * 3
        else
          @allowed_votes_count = base * 4
        end
      end
      @allowed_votes_count
    end
    def default_profile_link
      case self.profile
      when "twitter"
        "http://twitter.com/#{self.screen_name}"
      when "facebook"
        "http://facebook.com/#{self.screen_name}"
      end
    end
    def default_profile_display
      case self.profile
      when "twitter"
        "@#{self.screen_name}"
      when "facebook"
        "(f) #{self.screen_name}"
      end
    end
    def follow_qoloquio_if_doesnt
      raise "not ready."
      return
      if not self.follows_loxaesmas and not (self.twitter_access.blank? or self.twitter_secret.blank?)
        Twitter.configure do |config|
          config.oauth_token = self.twitter_access
          config.oauth_token_secret = self.twitter_secret
        end
        begin
          Twitter.follow 1164089670          # qoloquio id
          return true
        rescue
          return false
        end
      end
    end
    def link_to_default_profile
      if self.profile.downcase == "facebook"
        "<a href='#{self.default_profile_link}' target='_blank'>#{self.default_profile_display}</a>"
      elsif self.profile.downcase == "twitter"
        "<a href='#{self.default_profile_link}' target='_blank'>#{self.default_profile_display}</a>"
      end
    end
    def share_to_social_networks( obj )
      self.def_profile.share( obj )
    end
    def twitter_or_fb_nickname
      case self.profile
      when "twitter"
        "@#{self.nickname}"
      when "facebook"
        "#{self.name}"
      end
    end

  # class methods:
    def self.create_or_update_from_omniauth( omniauth_hash )
      if omniauth_hash["provider"] == "twitter"
        profile = TwitterProfile.create_or_update( omniauth_hash )
        if profile.new_record?
          user = User.create(profile: "twitter")
          profile.user = user
          profile.save
          return user
        else
          return profile.user
        end
      elsif omniauth_hash["provider"] == "facebook"
        profile = FacebookProfile.create_or_update( omniauth_hash )
        if profile.new_record?
          user = User.create(profile: "facebook")
          profile.user = user
          profile.save
          return user
        else
          return profile.user
        end
      else
        raise "Only FB and twitter are currently supported"
      end
    end

  # aliases:
    alias screen_name nickname
    alias default_profile def_profile

end
