# encoding: utf-8
class Pregunta < ActiveRecord::Base
  attr_accessible :titulo,
                  :detalle,
                  :enlace,
                  :dirigida_a

  attr_accessor :dirigida_a

  # relationships:
    belongs_to :user
    belongs_to :member
    has_reputation :votes,
                   source: :user,
                   source_of: [{ reputation: :user_votes, of: :user }],
                   aggregated_by: :sum

  # validations:
    validates :titulo, :presence => true
    validates :detalle, :presence => true
    # validates :dirigida_a, :presence => true
    validates :user_id, :presence => true
    validates :member_id, :presence => true
    validate :dirigida_a_has_one_valid_member

  # class methods:
    def self.candidatos_nombres
      Member.candidatos_with_lista
    end
    #def self.get_message_to_share
    #  messages = [
    #    "Recomiendo la pregunta"
    #  ]
    #  messages[(rand*messages.length).to_i]
    #end

  # instance methods:
    def dirigida_a=( name_with_lista )
      if name_with_lista.include? "("
        name_with_lista.slice!( (name_with_lista.index("(") - 1), (name_with_lista.length - name_with_lista.index("(") + 1) )
      end
      logger.debug "name: \n#{name_with_lista}"
      self.member = Member.find_by_nombre name_with_lista
    end
    def dirigida_a
      self.member.candidato_with_lista if self.member
    end
    

  private

    def dirigida_a_has_one_valid_member
      unless Member.exists?(id: self.member_id)
        errors.add :dirigida_a, "candidato no válido"
      end
    end

end
