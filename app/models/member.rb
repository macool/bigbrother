class Member < ActiveRecord::Base

  has_many :preguntas

  def self.candidatos_with_lista
    Member.all.map do |m|
        if m.lista.blank?
          m.nombre
        else
          "#{m.nombre} (#{m.lista})"
        end
      end
  end

  def candidato_with_lista
    text = "#{self.nombre}"
    unless self.lista.blank?
      text += " (#{self.lista})"
    end
    text
  end

  def nombre_or_nickname
    if self.username.blank?
      self.nombre
    else
      self.username
    end
  end

end
