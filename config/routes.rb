Bigbrother::Application.routes.draw do

  match '/auth/:provider/callback' => 'sessions#create' # omniauth
  
  get 'logout' => 'sessions#destroy', :as => "logout"
  get 'login' => 'sessions#index', :as => "login"

  get 'me' => 'users#me', as: "me"
  put 'me/update_preferences' => 'users#update_preferences', as: 'update_my_preferences'

  resources :sessions

  resources :preguntas do
    member do
      post :vote
      post :share
    end
    collection do
      get :me
    end
  end

  get '/faqs' => 'faqs#index', :as => "faqs"

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  root :to => 'preguntas#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
  match '*a', :to => 'application#routing_error'
end
