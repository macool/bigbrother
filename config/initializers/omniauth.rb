options = YAML.load(File.open(Rails.root.join('config', 'omniauth_credentials.yml')))

Rails.application.config.middleware.use OmniAuth::Builder do
  provider :facebook, options["facebook"]["access"], options["facebook"]["secret"], :scope => 'email,user_birthday,user_about_me,publish_stream,publish_actions,user_likes,offline_access'
  provider :twitter, options["twitter"]["access"], options["twitter"]["secret"]
end

# twitter intializer:
Twitter.configure do |config|
  config.consumer_key = options["twitter"]["access"]
  config.consumer_secret = options["twitter"]["secret"]
end

# facebook initializer:
# (doesnt exist yet)
