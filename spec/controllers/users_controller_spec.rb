require 'spec_helper'

describe UsersController do

  describe "GET 'me'" do
    it "returns http success" do
      get 'me'
      response.should be_success
    end
  end

  describe "GET 'update_preferences'" do
    it "returns http success" do
      get 'update_preferences'
      response.should be_success
    end
  end

end
